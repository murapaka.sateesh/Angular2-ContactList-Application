import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ContactListService } from './contact-list.service';
import {ContactListPipe} from './contact-list/contact-list.pipe';
import {ContactListCount} from './contact-list/contact-list.count';


import { AppComponent } from './app.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';
import {HttpModule} from '@angular/http';


@NgModule({
  declarations: [
    AppComponent,
    ContactListComponent,
    ContactDetailsComponent,
    MessagesComponent,
    ContactListPipe,
    ContactListCount
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [ContactListService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
