import {Contact} from  './contact';

export const CONTACTS:Contact[]=[
{
    id:1,
    name:'sateesh',
    phone:'8522922013',
    gender:'Male'
},
{
    id:2,
    name:'Divya',
    phone:'7729936067',
    gender:'Female'
},
{
    id:3,
    name:'Pavan',
    phone:'8500888609',
    gender:'Male'
},
{
    id:4,
    name:'Sharma',
    phone:'7799424439',
    gender:'Male'
}
];