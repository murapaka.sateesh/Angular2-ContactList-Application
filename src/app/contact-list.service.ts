import { Injectable } from '@angular/core';
import { Contact } from './contact';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { CONTACTS } from './mock-contacts';
import { MessageService } from './message.service';
//import { Http } from '@angular/http';

@Injectable()
export class ContactListService {
  //configurl='data1/contactList.json';
  constructor(private messageService: MessageService/*, private http:Http*/) { }
  getContactList():Observable<Contact[]> {
    //adding message only
    this.messageService.add('Fetched contactList Display Here');
    return of(CONTACTS);
    //return this.http.get(this.configurl);
  }

}
