import { Component,Input,Output, EventEmitter } from "@angular/core";

@Component({
    selector:'contact-list-count',
    templateUrl:'contact-list.count.html',
    styleUrls:['contact-list.component.css']
})
export class ContactListCount{

    selectedRadioButtonValue:string='All';

    @Output()
    countRadioButtonSelectionChanged:EventEmitter<string>=new EventEmitter<string>();

@Input()
All:number;

@Input()
Male:number;

@Input()
Female:number;

onRadioButtonSelectionChange(){
    this.countRadioButtonSelectionChanged.emit(this.selectedRadioButtonValue);
}
}

