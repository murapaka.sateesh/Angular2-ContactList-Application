import { Component, OnInit } from '@angular/core';
import { ContactListService } from '../contact-list.service';
import { Contact } from '../contact';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  contactList:Contact[];
  selectedContactRadioButtonValue:string='All';
  selectedContact: Contact;
  
  constructor(private contactListService:ContactListService, private messageService: MessageService) { }

  ngOnInit() {
    this.getContactList();
  }

  getContactList(){
    this.contactListService.getContactList()
    .subscribe(contactList=>this.contactList=contactList);
    //.subscribe(data=>this.contactList);
  }

  getAllContactList():number{
    return this.contactList.length;
  }
  getMaleContactList():number{
    return this.contactList.filter(e=>e.gender==='Male').length;
  }
  getFemaleContactList():number{
    return this.contactList.filter(e=>e.gender==='Female').length;
  }
  onRadioButtonSelectionDataChange(selectedRadioButtonValue:string):void{
    this.selectedContactRadioButtonValue=selectedRadioButtonValue;
  }

  onClick(contactList:Contact):void{
    this.messageService.clear();
    this.selectedContact=contactList;
    this.messageService.add("Fetched Contact List for: "+this.selectedContact.name);
  }

}
